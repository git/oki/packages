#include "mimetype.h"

#include <string.h>

const char *guessMimeType(const char *filename) {
	const char *dot = strrchr(filename, '.');
	if (!dot || dot == filename) {
		return "application/octet-stream";
	}
	const char *extension = dot + 1;
	if (!extension) {
		return "text/plain";
	}

	if (strcmp("avif", extension) == 0) {
		return "image/avif";
	} else if (strcmp("css", extension) == 0) {
		return "text/css";
	} else if (strcmp("csv", extension) == 0) {
		return "text/csv";
	} else if (strcmp("gif", extension) == 0) {
		return "image/gif";
	} else if (strcmp("html", extension) == 0) {
		return "text/html";
	} else if (strcmp("jpg", extension) == 0 || strcmp("jpeg", extension) == 0) {
		return "image/jpeg";
	} else if (strcmp("js", extension) == 0) {
		return "application/javascript";
	} else if (strcmp("json", extension) == 0) {
		return "application/json";
	} else if (strcmp("pdf", extension) == 0) {
		return "application/pdf";
	} else if (strcmp("png", extension) == 0) {
		return "image/png";
	} else if (strcmp("svg", extension) == 0) {
		return "image/svg+xml";
	} else if (strcmp("webp", extension) == 0) {
		return "image/webp";
	}
	return "text/plain";
}
