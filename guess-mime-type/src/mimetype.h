#ifndef GUESS_MIME_TYPE_H
#define GUESS_MIME_TYPE_H

/**
 * Guess the mime type of a file from its extension.
 *
 * @param filename The file name
 * @return The mime type deduced from common web extensions
 */
const char *guessMimeType(const char *filename);

#endif // GUESS_MIME_TYPE_H
