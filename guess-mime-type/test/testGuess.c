#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "mimetype.h"

int main(void) {
	assert(strcmp(guessMimeType("/tmp/file"), "application/octet-stream") == 0);
	assert(strcmp(guessMimeType("www/car.avif"), "image/avif") == 0);
	assert(strcmp(guessMimeType("css.html"), "text/html") == 0);
	assert(strcmp(guessMimeType("html.css.js"), "application/javascript") == 0);
	printf("Success!\n");
	return 0;
}
