#include "escape.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main(void) {
	assert(strcmp("aa", shellEscape("aa")) == 0);
	assert(strcmp("'aa bb'", shellEscape("aa bb")) == 0);
	assert(strcmp("'a(b)'", shellEscape("a(b)")) == 0);
	assert(strcmp("'aa && bb'", shellEscape("aa && bb")) == 0);
	assert(strcmp("'echo aa '\"'\"'s'\"'\"''", shellEscape("echo aa 's'")) == 0);
	return 0;
}

