#ifndef SHELL_ESCAPE_H
#define SHELL_ESCAPE_H

const char *shellEscape(const char *str);

#endif // SHELL_ESCAPE_H
