#include "escape.h"

#include <string-builder.h>

#include <ctype.h>
#include <stdbool.h>
#include <string.h>

bool isShellSafe(char ch) {
    return strchr("@%+=:,./-_", ch) != NULL || isalnum(ch);
}

bool isSafeShellStr(const char *str) {
    for (; *str; str++) {
        if (!isShellSafe(*str)) {
            return false;
        }
    }
    return true;
}

const char *shellEscape(const char *str) {
    if (isSafeShellStr(str)) {
        return str;
    }

    char *result = tmp_end();

    tmp_append_char('\'');
    while (str) {
        char *end = strchr(str, '\'');
        if (end) {
            tmp_append_sized(str, end - str);
            tmp_append_cstr("'\"'\"'");
            str = end + 1;
        } else {
            tmp_append_cstr(str);
            str = NULL;
        }
    }
    tmp_append_char('\'');
    tmp_append_char('\0');

    return result;
}
