#include "mths.h"

#include <assert.h>
#include <stdio.h>

int main(void) {
	assert(MAX(5, 5) == 5);
	assert(MAX(7, 2) == 7);
	assert(MAX(4, 9) == 9);

	assert(MIN(5, 5) == 5);
	assert(MIN(7, 2) == 2);
	assert(MIN(4, 9) == 4);

	assert(LERP(0., 4., 0.) == 0.);
	assert(LERP(0., 4., 0.25) == 1.);
	assert(LERP(0., 4., 0.5) == 2.);
	assert(LERP(0., 4., 0.75) == 3.);
	assert(LERP(0., 4., 1.) == 4.);

	printf("Success!\n");
	return 0;
}
