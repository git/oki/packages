/** Get the maximum between two numbers */
#define MAX(x, y) ((x > y) ? x : y)

/** Get the minimum between two numbers */
#define MIN(x, y) ((x < y) ? x : y)

/** Linearly interpolate t between two points a and b */
#define LERP(a, b, t) ((b - a) * t + a)
