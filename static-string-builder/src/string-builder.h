#ifndef STATIC_STRING_BUILDER_H
#define STATIC_STRING_BUILDER_H

#include <stddef.h>

#ifndef TMP_CAP
#define TMP_CAP 1024
#endif

/**
 * Get a pointer on the byte just after the last character of the buffer.
 *
 * @return A char pointer
 */
char *tmp_end(void);

/**
 * Append a new byte at the end of the current string builder.
 *
 * @param c The character to add
 * @return A pointer just before the character was added
 */
char *tmp_append_char(char c);

char *tmp_append_sized(const char *buffer, size_t buffer_sz);

char *tmp_append_cstr(const char *cstr);

void tmp_clean();

void tmp_rewind(const char *end);

#endif // STATIC_STRING_BUILDER_H
