#include "string-builder.h"

#include <assert.h>
#include <string.h>

char tmp[TMP_CAP] = {};
size_t tmp_size = 0;

char *tmp_end(void) {
    return tmp + tmp_size;
}

char *tmp_alloc(size_t size) {
    assert(tmp_size + size <= TMP_CAP);
    char *result = tmp_end();
    tmp_size += size;
    return result;
}

char *tmp_append_char(char c) {
    assert(tmp_size < TMP_CAP);
    tmp[tmp_size++] = c;
    return tmp + tmp_size - 1;
}

char *tmp_append_sized(const char *buffer, size_t buffer_sz) {
    char *result = tmp_alloc(buffer_sz);
    return memcpy(result, buffer, buffer_sz);
}

char *tmp_append_cstr(const char *cstr) {
    return tmp_append_sized(cstr, strlen(cstr));
}

void tmp_clean() {
    tmp_size = 0;
}

void tmp_rewind(const char *end) {
    tmp_size = end - tmp;
}

