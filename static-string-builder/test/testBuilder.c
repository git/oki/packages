#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "string-builder.h"

int main(void) {
    char *result = tmp_end();
    assert(*result == '\0');

    tmp_append_char('a');
    assert(strcmp(result, "a") == 0);
    for (char c = 'b'; c <= 'z'; ++c) {
        tmp_append_char(c);
    }
    char *end = tmp_append_char('\0');
    assert(*end == '\0');
    assert(strcmp(result, "abcdefghijklmnopqrstuvwxyz") == 0);

    tmp_clean();
    result = tmp_end();
    tmp_append_cstr("Hello,");
    char *comma = tmp_append_cstr(" world!");
    tmp_append_char('\0');
    assert(strcmp(comma, " world!") == 0);
    assert(strcmp(result, "Hello, world!") == 0);

    tmp_rewind(comma);
    tmp_append_cstr(" the world!");
    tmp_append_char('\0');
    assert(strcmp(result, "Hello, the world!") == 0);

    printf("Success!\n");
    return 0;
}
