#ifndef MY_LINKED_LIST_H
#define MY_LINKED_LIST_H

typedef struct list_node {
	int value;
	struct list_node *next;
} *LinkedList;

LinkedList createLinkedList(void);

void insertAtListHead(LinkedList *list, int value);

int linkedListLength(LinkedList list);

void freeLinkedList(LinkedList *list);

#endif // MY_LINKED_LIST_H
