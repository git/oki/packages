#include "linkedList.h"

#include <stdio.h>
#include <stdlib.h>

LinkedList createLinkedList(void) {
	return NULL;
}

void insertAtListHead(LinkedList *list, int value) {
	struct list_node *node = malloc(sizeof(struct list_node));
	if (node == NULL) {
		fprintf(stderr, "Memory exhausted!\n");
		abort();
	}
	node->value = value;
	node->next = *list;
	*list = node;
}

int linkedListLength(LinkedList list) {
	int n = 0;
	while (list != NULL) {
		n += 1;
		list = list->next;
	}
	return n;
}

void freeLinkedList(LinkedList *list) {
	struct list_node *next;
	struct list_node *node = *list;
	while (node != NULL) {
		next = node->next;
		free(node);
		node = next;
	}
	*list = NULL;
}
