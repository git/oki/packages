#include "linkedList.h"

#include <assert.h>
#include <stdio.h>

int main(void) {
	LinkedList list = createLinkedList();
	insertAtListHead(&list, 5);
	insertAtListHead(&list, 9);
	assert(linkedListLength(list) == 2);
	freeLinkedList(&list);
	printf("Success!\n");
	return 0;
}
